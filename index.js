import express from 'express';
import { router as todoRouter } from './backend/todo/todo.routes.js';
import mongoose from 'mongoose';

const app = express();

mongoose.connect(`${process.env.DB_URL}/todolist`);

app.use(express.static('frontend'));

app.use(express.json());
app.use('/api/todos', todoRouter);

mongoose.connection.once('open', () => {
  console.log('Connected to MongoDB');
  const port = process.env.PORT;
  app.listen(port, () => {
    console.log(`Server listens on port ${port}`);
  });
});
